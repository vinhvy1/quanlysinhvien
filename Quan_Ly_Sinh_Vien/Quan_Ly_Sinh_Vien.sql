USE [Quan_Ly_Sinh_Vien]
GO
/****** Object:  Table [dbo].[tblDIEM_HK]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDIEM_HK](
	[MaSV] [nchar](20) NOT NULL,
	[HocKi] [nvarchar](30) NOT NULL,
	[NamBD] [int] NOT NULL,
	[NamKT] [int] NULL,
	[DiemRL] [int] NULL,
	[DiemTB] [float] NULL,
	[DiemHe4] [nchar](10) NULL,
	[XepLoai] [nvarchar](20) NULL,
 CONSTRAINT [PK_tblDIEM_HK] PRIMARY KEY CLUSTERED 
(
	[MaSV] ASC,
	[HocKi] ASC,
	[NamBD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDIEM_HP]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDIEM_HP](
	[MaSV] [nchar](20) NOT NULL,
	[MaLopHP] [nchar](20) NOT NULL,
	[DiemQT] [float] NULL,
	[DiemThiLan1] [float] NULL,
	[DiemTongKet] [float] NULL,
	[DiemHe4] [nchar](10) NULL,
	[GhiChu] [nvarchar](200) NULL,
 CONSTRAINT [PK_tblDIEM_HP] PRIMARY KEY CLUSTERED 
(
	[MaSV] ASC,
	[MaLopHP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGIANG_VIEN]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGIANG_VIEN](
	[MaGV] [nchar](15) NOT NULL,
	[TenGV] [nvarchar](50) NULL,
	[GioiTinh] [nvarchar](10) NULL,
	[Phone] [varchar](15) NULL,
	[Email] [varchar](50) NULL,
	[PhanLoaiGV] [nvarchar](20) NULL,
	[Anh] [nvarchar](200) NULL,
 CONSTRAINT [PK_tblGIAO_VIEN] PRIMARY KEY CLUSTERED 
(
	[MaGV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblKHOA]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblKHOA](
	[MaKhoa] [nchar](20) NOT NULL,
	[TenKhoa] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblKHOA] PRIMARY KEY CLUSTERED 
(
	[MaKhoa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLOGIN]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLOGIN](
	[TenDN] [nvarchar](50) NOT NULL,
	[MatKhau] [nvarchar](50) NOT NULL,
	[HoTen] [nvarchar](50) NULL,
	[GioiTinh] [nvarchar](10) NULL,
	[Phone] [varchar](15) NULL,
	[Email] [varchar](50) NULL,
	[MaQuyen] [nchar](20) NULL,
 CONSTRAINT [PK_tblLOGIN] PRIMARY KEY CLUSTERED 
(
	[TenDN] ASC,
	[MatKhau] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLOP]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLOP](
	[MaLop] [nchar](20) NOT NULL,
	[MaKhoa] [nchar](20) NULL,
	[TenLop] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblLOP] PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLOP_HOC_PHAN]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLOP_HOC_PHAN](
	[MaLopHP] [nchar](20) NOT NULL,
	[SoDVHT] [int] NULL,
	[SoTC] [int] NULL,
	[HocKi] [nvarchar](30) NULL,
	[NamBD] [int] NULL,
	[NamKT] [int] NULL,
	[MaMon] [nchar](20) NULL,
	[MaGV] [nchar](15) NULL,
	[MaLop] [nchar](20) NULL,
 CONSTRAINT [PK_tblLOP_HOC_PHAN] PRIMARY KEY CLUSTERED 
(
	[MaLopHP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMON]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMON](
	[MaMon] [nchar](20) NOT NULL,
	[TenMon] [nvarchar](50) NULL,
	[MaKhoa] [nchar](20) NULL,
 CONSTRAINT [PK_tblMON] PRIMARY KEY CLUSTERED 
(
	[MaMon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblQuyen]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblQuyen](
	[MaQuyen] [nchar](20) NOT NULL,
	[Quyen] [nvarchar](30) NULL,
 CONSTRAINT [PK_tblQuyen] PRIMARY KEY CLUSTERED 
(
	[MaQuyen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSINH_VIEN]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSINH_VIEN](
	[MaSV] [nchar](20) NOT NULL,
	[HoTen] [nvarchar](50) NULL,
	[NgaySinh] [date] NULL,
	[GioiTinh] [nvarchar](10) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[MaLop] [nchar](20) NULL,
 CONSTRAINT [PK_tblSINH_VIEN] PRIMARY KEY CLUSTERED 
(
	[MaSV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblDIEM_HK] ([MaSV], [HocKi], [NamBD], [NamKT], [DiemRL], [DiemTB], [DiemHe4], [XepLoai]) VALUES (N'17103100324         ', N'1', 2019, 2020, 100, 8.4, N'B         ', N'Khá')
INSERT [dbo].[tblDIEM_HK] ([MaSV], [HocKi], [NamBD], [NamKT], [DiemRL], [DiemTB], [DiemHe4], [XepLoai]) VALUES (N'17103100326         ', N'1', 2019, 2020, 70, 6.2, N'C         ', N'Trung bình')
INSERT [dbo].[tblDIEM_HK] ([MaSV], [HocKi], [NamBD], [NamKT], [DiemRL], [DiemTB], [DiemHe4], [XepLoai]) VALUES (N'17103100327         ', N'1', 2019, 2020, 71, 7.94, N'B         ', N'Khá')
INSERT [dbo].[tblDIEM_HK] ([MaSV], [HocKi], [NamBD], [NamKT], [DiemRL], [DiemTB], [DiemHe4], [XepLoai]) VALUES (N'17103100328         ', N'1', 2019, 2020, 80, 4.8, N'D         ', N'Yếu')
INSERT [dbo].[tblDIEM_HK] ([MaSV], [HocKi], [NamBD], [NamKT], [DiemRL], [DiemTB], [DiemHe4], [XepLoai]) VALUES (N'17103100329         ', N'1', 2019, 2020, 70, 5, N'D         ', N'Yếu')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100324         ', N'0101000             ', 9, 10, 9.6, N'A         ', N'')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100324         ', N'0101001             ', 10, 9, 9.4, N'A         ', N'')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100324         ', N'0101002             ', 9, 6, 7.2, N'B         ', N'')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100326         ', N'0101000             ', 5, 7, 6.2, N'C         ', N'')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100326         ', N'01010001            ', 3, 2, 2.4, N'F         ', N'Học lại')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100326         ', N'0101002             ', 3, 7, 5.4, N'D         ', N'')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100327         ', N'0101000             ', 7.1, 8.5, 7.94, N'B         ', N'')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100328         ', N'0101000             ', 6, 4, 4.8, N'D         ', N'')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100329         ', N'0101000             ', 3, 2, 2.4, N'F         ', N'Học lại')
INSERT [dbo].[tblDIEM_HP] ([MaSV], [MaLopHP], [DiemQT], [DiemThiLan1], [DiemTongKet], [DiemHe4], [GhiChu]) VALUES (N'17103100329         ', N'0101002             ', 5, 5, 5, N'D         ', N'')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV01           ', N'Nguyễn Bích Phương', N'Nữ', N'0845345455', N'bichphuong@gmail.com', N'Thỉnh giảng', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\my-nhan-cuoi-dep-nhat-xu-han-han-hyo-joo-co-vu-moi-nguoi-vuot-qua-dich-benh-705-4787175.jpg')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV02           ', N'Đỗ Bảo Anh', N'Nữ', N'0934232445', N'baoanh@gmail.com', N'Thỉnh giảng', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\Jh5nx5p39yhwdg5rqqaa.jpg')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV03           ', N'Hoàng Yến', N'Nữ', N'0934234323', N'hoangyen@gmail.com', N'Cơ hữu', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\img2-15751648821851354482571-crop-15751648893251453788890.jpg')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV04           ', N'Nguyễn Khánh Linh', N'Nữ', N'0943423437', N'khanhlinh@gmail.com', N'Thỉnh giảng', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\photo1592452665217-15924526654061396759367.jpg')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV05           ', N'Dương Hoàng Yến', N'Nữ', N'0934234324', N'duonghy@gmail.com', N'Cơ hữu', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\tieu-su-ca-si-duong-hoang-yen-466327.jpg')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV06           ', N'Hồ Thị Hiền', N'Nữ', N'0923432433', N'hienho@gmail,com', N'Thỉnh giảng', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\hien-ho.jpg')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV07           ', N'Ninh Dương Lan Ngọc', N'Nữ', N'0923432434', N'lanngoc@gmail.com', N'Thỉnh giảng', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\8813022933664118333741585167223992167170048o-15829376324211375224104-crop-1582937948777565198621.jpg')
INSERT [dbo].[tblGIANG_VIEN] ([MaGV], [TenGV], [GioiTinh], [Phone], [Email], [PhanLoaiGV], [Anh]) VALUES (N'GV09           ', N'Diễm My', N'Nữ', N'0945345345', N'diemmy@gmail.com', N'Cơ hữu', N'E:\Hoc tap\C#\Quan_Ly_Sinh_Vien\anh\Diem-My-9X-3-01.jpg')
INSERT [dbo].[tblKHOA] ([MaKhoa], [TenKhoa]) VALUES (N'cntt                ', N'Công nghệ thông tin')
INSERT [dbo].[tblKHOA] ([MaKhoa], [TenKhoa]) VALUES (N'DT                  ', N'Điện tử')
INSERT [dbo].[tblKHOA] ([MaKhoa], [TenKhoa]) VALUES (N'KT                  ', N'Kế toán')
INSERT [dbo].[tblLOGIN] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Phone], [Email], [MaQuyen]) VALUES (N'admin', N'12354', N'Hoàng Đăng Vinh', N'Nam', N'0976522222', N'hoangvinh@gmail.com', N'1                   ')
INSERT [dbo].[tblLOGIN] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Phone], [Email], [MaQuyen]) VALUES (N'dangvinh', N'12354', N'Hoàng Đăng Vinh', N'Nam', N'03243243432', N'hoangvinh@gmail.com', N'3                   ')
INSERT [dbo].[tblLOGIN] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Phone], [Email], [MaQuyen]) VALUES (N'nguyenquoctien', N'12354', N'Nguyễn Quốc Tiến', N'Nam', N'0323434345', N'quoctien@gmail.com', N'2                   ')
INSERT [dbo].[tblLOGIN] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Phone], [Email], [MaQuyen]) VALUES (N'nguyenthanhnam', N'12354', N'Nguyễn Thành Nam', N'Nam', N'083424324', N'thanhnam@gmail.com', N'1                   ')
INSERT [dbo].[tblLOGIN] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Phone], [Email], [MaQuyen]) VALUES (N'tiendz', N'12354', N'Nguyễn Quốc Tiến', N'Nam', N'0234343243', N'tiendz@gmail.com', N'1                   ')
INSERT [dbo].[tblLOGIN] ([TenDN], [MatKhau], [HoTen], [GioiTinh], [Phone], [Email], [MaQuyen]) VALUES (N'trandoanthuc', N'thucoccho', N'Trần Doãn Thực', N'Nam', N'0983243244', N'doanthuc@gmail.com', N'1                   ')
INSERT [dbo].[tblLOP] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'dhti11a2            ', N'cntt                ', N'Tin 11a2')
INSERT [dbo].[tblLOP] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'DT11A1HN            ', N'DT                  ', N'Điện tử 11a1')
INSERT [dbo].[tblLOP] ([MaLop], [MaKhoa], [TenLop]) VALUES (N'KT11A1HN            ', N'KT                  ', N'Kế toán 11 a1')
INSERT [dbo].[tblLOP_HOC_PHAN] ([MaLopHP], [SoDVHT], [SoTC], [HocKi], [NamBD], [NamKT], [MaMon], [MaGV], [MaLop]) VALUES (N'0101000             ', 60, 4, N'1', 2019, 2020, N'MH001               ', N'GV02           ', N'dhti11a2            ')
INSERT [dbo].[tblLOP_HOC_PHAN] ([MaLopHP], [SoDVHT], [SoTC], [HocKi], [NamBD], [NamKT], [MaMon], [MaGV], [MaLop]) VALUES (N'01010001            ', 45, 3, N'1', 2019, 2020, N'MH005               ', N'GV05           ', N'dhti11a2            ')
INSERT [dbo].[tblLOP_HOC_PHAN] ([MaLopHP], [SoDVHT], [SoTC], [HocKi], [NamBD], [NamKT], [MaMon], [MaGV], [MaLop]) VALUES (N'0101001             ', 45, 3, N'2', 2019, 2020, N'MH002               ', N'GV06           ', N'KT11A1HN            ')
INSERT [dbo].[tblLOP_HOC_PHAN] ([MaLopHP], [SoDVHT], [SoTC], [HocKi], [NamBD], [NamKT], [MaMon], [MaGV], [MaLop]) VALUES (N'0101002             ', 45, 3, N'1', 2019, 2020, N'MH003               ', N'GV07           ', N'dhti11a2            ')
INSERT [dbo].[tblMON] ([MaMon], [TenMon], [MaKhoa]) VALUES (N'MH001               ', N'Tin cơ sở', N'cntt                ')
INSERT [dbo].[tblMON] ([MaMon], [TenMon], [MaKhoa]) VALUES (N'MH002               ', N'Vật lý 1', N'DT                  ')
INSERT [dbo].[tblMON] ([MaMon], [TenMon], [MaKhoa]) VALUES (N'MH003               ', N'Lập trình hướng đối tượng', N'cntt                ')
INSERT [dbo].[tblMON] ([MaMon], [TenMon], [MaKhoa]) VALUES (N'MH004               ', N'Tin văn phòng', N'KT                  ')
INSERT [dbo].[tblMON] ([MaMon], [TenMon], [MaKhoa]) VALUES (N'MH005               ', N'Đại số tuyến tính', N'cntt                ')
INSERT [dbo].[tblQuyen] ([MaQuyen], [Quyen]) VALUES (N'1                   ', N'admin')
INSERT [dbo].[tblQuyen] ([MaQuyen], [Quyen]) VALUES (N'2                   ', N'giangvien')
INSERT [dbo].[tblQuyen] ([MaQuyen], [Quyen]) VALUES (N'3                   ', N'sinhvien')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100324         ', N'Hoàng Đăng Vinh', CAST(N'1999-11-05' AS Date), N'Nam', N'Bắc Giang', N'dhti11a2            ')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100325         ', N'Thân Thị Anh', CAST(N'1999-11-18' AS Date), N'Nữ', N'Bắc Giang', N'KT11A1HN            ')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100326         ', N'Trần Doãn Thực', CAST(N'1999-11-18' AS Date), N'Nam', N'Hưng Yên', N'dhti11a2            ')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100327         ', N'Nguyễn Quốc Tiến', CAST(N'1999-11-05' AS Date), N'Nam', N'Hà Nội', N'dhti11a2            ')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100328         ', N'Nguyễn Thái  Sơn', CAST(N'1999-03-18' AS Date), N'Nam', N'Hải Dương', N'dhti11a2            ')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100329         ', N'Nguyễn Thành Nam', CAST(N'1999-11-18' AS Date), N'Nam', N'Hà Nội', N'dhti11a2            ')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100330         ', N'Nguyễn Công Phượng', CAST(N'1995-11-26' AS Date), N'Nam', N'Nghệ An', N'DT11A1HN            ')
INSERT [dbo].[tblSINH_VIEN] ([MaSV], [HoTen], [NgaySinh], [GioiTinh], [DiaChi], [MaLop]) VALUES (N'17103100331         ', N'Nguyễn Quang Hải', CAST(N'1997-11-26' AS Date), N'Nam', N'Hà Nội', N'DT11A1HN            ')
ALTER TABLE [dbo].[tblDIEM_HK]  WITH CHECK ADD  CONSTRAINT [FK_tblDIEM_HK_tblSINH_VIEN] FOREIGN KEY([MaSV])
REFERENCES [dbo].[tblSINH_VIEN] ([MaSV])
GO
ALTER TABLE [dbo].[tblDIEM_HK] CHECK CONSTRAINT [FK_tblDIEM_HK_tblSINH_VIEN]
GO
ALTER TABLE [dbo].[tblDIEM_HP]  WITH CHECK ADD  CONSTRAINT [FK_tblDIEM_HP_tblLOP_HOC_PHAN] FOREIGN KEY([MaLopHP])
REFERENCES [dbo].[tblLOP_HOC_PHAN] ([MaLopHP])
GO
ALTER TABLE [dbo].[tblDIEM_HP] CHECK CONSTRAINT [FK_tblDIEM_HP_tblLOP_HOC_PHAN]
GO
ALTER TABLE [dbo].[tblDIEM_HP]  WITH CHECK ADD  CONSTRAINT [FK_tblDIEM_HP_tblSINH_VIEN] FOREIGN KEY([MaSV])
REFERENCES [dbo].[tblSINH_VIEN] ([MaSV])
GO
ALTER TABLE [dbo].[tblDIEM_HP] CHECK CONSTRAINT [FK_tblDIEM_HP_tblSINH_VIEN]
GO
ALTER TABLE [dbo].[tblLOGIN]  WITH CHECK ADD  CONSTRAINT [FK_tblLOGIN_tblQuyen] FOREIGN KEY([MaQuyen])
REFERENCES [dbo].[tblQuyen] ([MaQuyen])
GO
ALTER TABLE [dbo].[tblLOGIN] CHECK CONSTRAINT [FK_tblLOGIN_tblQuyen]
GO
ALTER TABLE [dbo].[tblLOP]  WITH CHECK ADD  CONSTRAINT [FK_tblLOP_tblKHOA] FOREIGN KEY([MaKhoa])
REFERENCES [dbo].[tblKHOA] ([MaKhoa])
GO
ALTER TABLE [dbo].[tblLOP] CHECK CONSTRAINT [FK_tblLOP_tblKHOA]
GO
ALTER TABLE [dbo].[tblLOP_HOC_PHAN]  WITH CHECK ADD  CONSTRAINT [FK_tblLOP_HOC_PHAN_tblGIANG_VIEN] FOREIGN KEY([MaGV])
REFERENCES [dbo].[tblGIANG_VIEN] ([MaGV])
GO
ALTER TABLE [dbo].[tblLOP_HOC_PHAN] CHECK CONSTRAINT [FK_tblLOP_HOC_PHAN_tblGIANG_VIEN]
GO
ALTER TABLE [dbo].[tblLOP_HOC_PHAN]  WITH CHECK ADD  CONSTRAINT [FK_tblLOP_HOC_PHAN_tblLOP] FOREIGN KEY([MaLop])
REFERENCES [dbo].[tblLOP] ([MaLop])
GO
ALTER TABLE [dbo].[tblLOP_HOC_PHAN] CHECK CONSTRAINT [FK_tblLOP_HOC_PHAN_tblLOP]
GO
ALTER TABLE [dbo].[tblLOP_HOC_PHAN]  WITH CHECK ADD  CONSTRAINT [FK_tblLOP_HOC_PHAN_tblMON] FOREIGN KEY([MaMon])
REFERENCES [dbo].[tblMON] ([MaMon])
GO
ALTER TABLE [dbo].[tblLOP_HOC_PHAN] CHECK CONSTRAINT [FK_tblLOP_HOC_PHAN_tblMON]
GO
ALTER TABLE [dbo].[tblSINH_VIEN]  WITH CHECK ADD  CONSTRAINT [FK_tblSINH_VIEN_tblLOP] FOREIGN KEY([MaLop])
REFERENCES [dbo].[tblLOP] ([MaLop])
GO
ALTER TABLE [dbo].[tblSINH_VIEN] CHECK CONSTRAINT [FK_tblSINH_VIEN_tblLOP]
GO
/****** Object:  StoredProcedure [dbo].[BCSVDuocHocBong]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BCSVDuocHocBong]
	@HocKi nvarchar(30),
	@NamBD int
AS
	SELECT A.MaSV, A.HoTen, A.NgaySinh, A.GioiTinh, A.DiaChi, A.MaLop, B.DiemRL, B.DiemTB, B.XepLoai, B.HocKi, B.NamBD, B.NamKT 
	FROM tblSINH_VIEN as A, tblDiem_HK as B
	WHERE A.MaSV = B.MaSV and b.DiemRL > 70 and b.DiemTB > 7 and b.HocKi = @HocKi and b.NamBD = @NamBD order by a.MaLop
GO
/****** Object:  StoredProcedure [dbo].[BCSVHocLai]    Script Date: 11/26/2020 5:31:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BCSVHocLai]
	@HocKi nvarchar(30),
	@NamBD int
AS
	SELECT B.MaLopHP,D.TenMon, A.MaSV, A.HoTen, A.NgaySinh, A.GioiTinh, A.DiaChi,  B.DiemTongKet,  C.HocKi, C.NamBD, C.NamKT 
	FROM tblSINH_VIEN as A, tblDIEM_HP as B, tblLOP_HOC_PHAN as C, tblMON as D
	WHERE A.MaSV = B.MaSV and B.MaLopHP = C.MaLopHP and C.MaMon = D.MaMon and B.DiemTongKet < 4 and C.HocKi =@HocKi and C.NamBD = @NamBD order by b.MaLopHP
GO
