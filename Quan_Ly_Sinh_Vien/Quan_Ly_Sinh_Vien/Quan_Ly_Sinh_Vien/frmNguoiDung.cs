﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmNguoiDung : Form
    {
        public frmNguoiDung()
        {
            InitializeComponent();
        }

        DataTable tblND;
        private void btnThem_Click(object sender, EventArgs e)
        {
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox
            txtHoTen.Enabled = true; //cho phép nhập mới
            txtHoTen.Focus();
        }

        private void ResetValue()
        {
            txtTenTK.Text = "";
            txtHoTen.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtMatKhau.Text = "";
            txtNhapLai.Text = "";
            cboGioiTinh.Text = "";
            cboQuyen.Text = "";
        }
        private void frmNguoiDung_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtHoTen.Enabled = false;
            LoadDataGridView();
            string sql = "select Quyen, MaQuyen from tblQuyen";
            Function.FillCombo(sql, cboQuyen, "MaQuyen", "Quyen");
        }

        private void LoadDataGridView()
        {
            string sql = "select TenDN, MatKhau, HoTen, GioiTinh, Phone, Email, Quyen from tblLOGIN, tblQuyen Where tblLOGIN.MaQuyen = tblQuyen.MaQuyen";
            tblND = Function.GetDataToTable(sql);
            dgvNguoiDung.DataSource = tblND;
            dgvNguoiDung.Columns[0].HeaderText = "Tên đăng nhập";
            dgvNguoiDung.Columns[1].HeaderText = "Mật khẩu";
            dgvNguoiDung.Columns[2].HeaderText = "Họ tên";
            dgvNguoiDung.Columns[3].HeaderText = "Giới tính";
            dgvNguoiDung.Columns[4].HeaderText = "Phone";
            dgvNguoiDung.Columns[5].HeaderText = "Email";
            dgvNguoiDung.Columns[6].HeaderText = "Quyền";
            dgvNguoiDung.AllowUserToAddRows = false;
            dgvNguoiDung.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            string maquyen;

            if (txtHoTen.Text.Trim().Length == 0) 
            {
                MessageBox.Show("Bạn phải nhập họ tên", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtHoTen.Focus();
                return;
            }
            if(cboGioiTinh.Text == "")
            {
                MessageBox.Show("Bạn phải nhập giới tính", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboGioiTinh.Focus();
                return;
            }    
            if (txtPhone.Text.Trim().Length == 0) 
            {
                MessageBox.Show("Bạn phải nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPhone.Focus();
                return;
            }

            if (txtEmail.Text.Trim().Length == 0) 
            {
                MessageBox.Show("Bạn phải nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEmail.Focus();
                return;
            }
            if (txtTenTK.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên tài khoản", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenTK.Focus();
                return;
            }
            if (txtMatKhau.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMatKhau.Focus();
                return;
            }
            if (txtNhapLai.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập lại mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNhapLai.Focus();
                return;
            }
            if (cboQuyen.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải chon quyền", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboQuyen.Focus();
                return;
            }
            if(txtMatKhau.Text != txtNhapLai.Text)
            {
                MessageBox.Show("Nhập lại mật khẩu chưa đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNhapLai.Focus();
                return;
            }    
            sql = "Select TenDN From tblLOGIN where TenDN = N'" + txtTenTK.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Tên tài khoản này đã có, bạn phải nhập tài khoản khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTenTK.Focus();
                return;
            }
            
            if (cboQuyen.Text == "admin")
                maquyen = "1";
            else if (cboQuyen.Text == "giangvien")
                maquyen = "2";
            else
                maquyen = "3";
            sql = "INSERT INTO tblLOGIN VALUES(N'"+txtTenTK.Text+"', '"+txtMatKhau.Text+"', N'"+txtHoTen.Text+"', N'"+cboGioiTinh.Text+"', '"+txtPhone.Text+"','"+txtEmail.Text+"','"+maquyen+"')";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtHoTen.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql;
            string maquyen;
            if (txtHoTen.Text.Trim().Length == 0) 
            {
                MessageBox.Show("Bạn phải nhập họ tên", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtHoTen.Focus();
                return;
            }
            if (cboGioiTinh.Text == "")
            {
                MessageBox.Show("Bạn phải nhập giới tính", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboGioiTinh.Focus();
                return;
            }
            if (txtPhone.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPhone.Focus();
                return;
            }

            if (txtEmail.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEmail.Focus();
                return;
            }
            if (txtTenTK.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên tài khoản", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenTK.Focus();
                return;
            }
            if (txtMatKhau.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMatKhau.Focus();
                return;
            }
            if (txtNhapLai.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập lại mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNhapLai.Focus();
                return;
            }
            if (cboQuyen.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải chon quyền", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboQuyen.Focus();
                return;
            }
            if (txtMatKhau.Text != txtNhapLai.Text)
            {
                MessageBox.Show("Nhập lại mật khẩu chưa đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNhapLai.Focus();
                return;
            }

            if (cboQuyen.Text == "admin")
                maquyen = "1";
            else if (cboQuyen.Text == "giangvien")
                maquyen = "2";
            else
                maquyen = "3";
            sql = "UPDATE tblLOGIN SET MatKhau = '" + txtMatKhau.Text + "', HoTen = N'" + txtHoTen.Text + "', GioiTinh = N'" + cboGioiTinh.Text + "', Phone = '" + txtPhone.Text + "',Email = '" + txtEmail.Text + "',MaQuyen = '" + maquyen + "' where TenDN = N'"+txtTenTK.Text + "'";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtHoTen.Enabled = false;
        }

        private void dgvNguoiDung_Click(object sender, EventArgs e)
        {
            if(btnThem.Enabled == false)
            {
                MessageBox.Show("Đang ở chế độ thêm mới!");
                return;
            }
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            txtHoTen.Text = dgvNguoiDung.CurrentRow.Cells["HoTen"].Value.ToString();
            cboGioiTinh.Text = dgvNguoiDung.CurrentRow.Cells["GioiTinh"].Value.ToString();
            txtPhone.Text = dgvNguoiDung.CurrentRow.Cells["Phone"].Value.ToString();
            txtEmail.Text = dgvNguoiDung.CurrentRow.Cells["Email"].Value.ToString();
            txtTenTK.Text = dgvNguoiDung.CurrentRow.Cells["TenDN"].Value.ToString();
            txtMatKhau.Text = dgvNguoiDung.CurrentRow.Cells["MatKhau"].Value.ToString();
            txtNhapLai.Text = dgvNguoiDung.CurrentRow.Cells["MatKhau"].Value.ToString();
            cboQuyen.Text = dgvNguoiDung.CurrentRow.Cells["Quyen"].Value.ToString();
            btnBoQua.Enabled = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblND.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtTenTK.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE tblLOGIN WHERE TenDN = N'" + txtTenTK.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            txtHoTen.Enabled = false;
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtHoTen.Enabled = false;
            btnThem.Enabled = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
