﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmTKDiem : Form
    {
        public frmTKDiem()
        {
            InitializeComponent();
        }
        DataTable tblDiem;
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            

            if (txtMaSV.Text == "" && txtMaLopHP.Text == "")
            {
                MessageBox.Show("Hãy chọn một điều kiện tìm kiếm!!!", "Yêu cầu ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string sql = "select a.MaSV, b.HoTen, c.MaLopHP, d.TenMon, a.DiemQT, a.DiemThiLan1, a.DiemTongKet, a.DiemHe4, c.HocKi, c.NamBD,c.NamKT from tblDIEM_HP as a , tblSINH_VIEN as b, tblLOP_HOC_PHAN as c, tblMON as d where a.MaSV = b.MaSV and a.MaLopHP = c.MaLopHP and c.MaMon = d.MaMon";    
            if (txtMaSV.Text != "")
                sql = sql + " AND a.MaSV Like N'%" + txtMaSV.Text + "%'";
            if (txtMaLopHP.Text != "")
                sql = sql + " AND a.MaLopHP Like N'%" + txtMaLopHP.Text + "%'";
            tblDiem = Function.GetDataToTable(sql);
            if (tblDiem.Rows.Count == 0)
            {
                MessageBox.Show("Không có bản ghi thỏa mãn điều kiện!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Có " + tblDiem.Rows.Count + " bản ghi thỏa mãn điều kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dgvDanhSach.DataSource = tblDiem;
            dgvDanhSach.Columns[0].HeaderText = "Mã sinh viên";
            dgvDanhSach.Columns[1].HeaderText = "Họ tên";
            dgvDanhSach.Columns[2].HeaderText = "Mã lớp hp";
            dgvDanhSach.Columns[3].HeaderText = "Tên môn";
            dgvDanhSach.Columns[4].HeaderText = "Điểm quá trình";
            dgvDanhSach.Columns[5].HeaderText = "Điểm thi";
            dgvDanhSach.Columns[6].HeaderText = "Điểm tổng kết";
            dgvDanhSach.Columns[7].HeaderText = "Điểm hệ 4";
            dgvDanhSach.Columns[8].HeaderText = "Học kì";
            dgvDanhSach.Columns[9].HeaderText = "Năm BD";
            dgvDanhSach.Columns[10].HeaderText = "Năm KT";
            dgvDanhSach.AllowUserToAddRows = false;
            dgvDanhSach.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void frmTKDiem_Load(object sender, EventArgs e)
        {
            
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
