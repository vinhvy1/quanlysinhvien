﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLGiangVien : Form
    {
        public frmQLGiangVien()
        {
            InitializeComponent();
        }

        DataTable tblGV;
        private void frmQLGiangVien_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaGV.Enabled = false;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            LoadDataGridView();
        }

        private void LoadDataGridView()
        {
            string sql = "select * from tblGIANG_VIEN";
            tblGV = Function.GetDataToTable(sql);
            dgvGiangVien.DataSource = tblGV;
            dgvGiangVien.Columns[0].HeaderText = "Mã giảng viên";
            dgvGiangVien.Columns[1].HeaderText = "Họ tên";
            dgvGiangVien.Columns[2].HeaderText = "Giới tính";
            dgvGiangVien.Columns[3].HeaderText = "Phone";
            dgvGiangVien.Columns[4].HeaderText = "Email";
            dgvGiangVien.Columns[5].HeaderText = "Phân loại";
            dgvGiangVien.Columns[6].HeaderText = "Ảnh";
            dgvGiangVien.AllowUserToAddRows = false;
            dgvGiangVien.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox
            txtMaGV.Enabled = true; //cho phép nhập mới
            txtMaGV.Focus();
        }

        private void ResetValue()
        {
            txtMaGV.Text = "";
            txtHoTen.Text = "";
            cboGioiTinh.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtAnh.Text = "";
            cboPhanLoai.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtMaGV.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã giảng viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaGV.Focus();
                return;
            }
            if (txtHoTen.Text == "")
            {
                MessageBox.Show("Bạn phải nhập họ tên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtHoTen.Focus();
                return;
            }
            if (cboGioiTinh.Text == "")
            {
                MessageBox.Show("Bạn phải nhập giới tính!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboGioiTinh.Focus();
                return;
            }
            if (txtPhone.Text == "")
            {
                MessageBox.Show("Bạn phải nhập số điện thoại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPhone.Focus();
                return;
            }
            if (txtEmail.Text == "")
            {
                MessageBox.Show("Bạn phải email!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEmail.Focus();
                return;
            }   
            if(cboPhanLoai.Text == "")
            {
                MessageBox.Show("Bạn phải nhập phân loại giáo viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboPhanLoai.Focus();
                return;
            }
            if(txtAnh.Text == "")
            {
                MessageBox.Show("Bạn phải nhập ảnh!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAnh.Focus();
                return;
            }    
            sql = "Select MaGV From tblGIANG_VIEN where MaGV = N'" + txtMaGV.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Mã giảng viên này đã có, bạn phải nhập mã khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMaGV.Focus();
                return;
            }

            sql = "INSERT INTO tblGIANG_VIEN Values(N'" + txtMaGV.Text + "' , N'" + txtHoTen.Text + "', N'" + cboGioiTinh.Text + "', '"+txtPhone.Text+"', '"+txtEmail.Text+"', N'"+cboPhanLoai.Text+"', N'"+txtAnh.Text+"')";

            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaGV.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtMaGV.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã giảng viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaGV.Focus();
                return;
            }
            if (txtHoTen.Text == "")
            {
                MessageBox.Show("Bạn phải nhập họ tên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtHoTen.Focus();
                return;
            }
            if (cboGioiTinh.Text == "")
            {
                MessageBox.Show("Bạn phải nhập giới tính!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboGioiTinh.Focus();
                return;
            }
            if (txtPhone.Text == "")
            {
                MessageBox.Show("Bạn phải nhập số điện thoại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPhone.Focus();
                return;
            }
            if (txtEmail.Text == "")
            {
                MessageBox.Show("Bạn phải email!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEmail.Focus();
                return;
            }
            if (cboPhanLoai.Text == "")
            {
                MessageBox.Show("Bạn phải nhập phân loại giáo viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboPhanLoai.Focus();
                return;
            }
            if (txtAnh.Text == "")
            {
                MessageBox.Show("Bạn phải nhập ảnh!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAnh.Focus();
                return;
            }

            sql = "update tblGIANG_VIEN set TenGV = N'" + txtHoTen.Text + "', GioiTinh = N'" + cboGioiTinh.Text + "',Phone = N'"+txtPhone.Text+"', Email = '"+txtEmail.Text+"', PhanLoaiGV = N'"+cboPhanLoai.Text+"', Anh = N'"+txtAnh.Text+"' where MaGV = '" + txtMaGV.Text + "'";
            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaGV.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblGV.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMaGV.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE tblGIANG_VIEN WHERE MaGV = N'" + txtMaGV.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
            btnThem.Enabled = true;
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
            btnBoQua.Enabled = false;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaGV.Enabled = false;
            btnThem.Enabled = true;
            pictureBox1.Image = null;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvGiangVien_Click(object sender, EventArgs e)
        {
            txtMaGV.Text = dgvGiangVien.CurrentRow.Cells["MaGV"].Value.ToString();
            txtHoTen.Text = dgvGiangVien.CurrentRow.Cells["TenGV"].Value.ToString();
            cboGioiTinh.Text = dgvGiangVien.CurrentRow.Cells["GioiTinh"].Value.ToString();
            txtPhone.Text = dgvGiangVien.CurrentRow.Cells["Phone"].Value.ToString();
            txtEmail.Text = dgvGiangVien.CurrentRow.Cells["Email"].Value.ToString();
            cboPhanLoai.Text = dgvGiangVien.CurrentRow.Cells["PhanLoaiGV"].Value.ToString();
            txtAnh.Text = dgvGiangVien.CurrentRow.Cells["Anh"].Value.ToString();
            try
            {
                pictureBox1.Image = Image.FromFile(txtAnh.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            btnThem.Enabled = false;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoQua.Enabled = true;
        }

        private void btnMo_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog();
            dlgOpen.Filter = "Bitmap(*.bmp)|*.bmp|JPEG(*.jpg)|*.jpg|GIF(*.gif)|*.gif|All files(*.*)|*.*";
            dlgOpen.FilterIndex = 2;
            dlgOpen.Title = "Chọn ảnh giáo viên";
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(dlgOpen.FileName);
                txtAnh.Text = dlgOpen.FileName;
            }
        }
    }
}
