﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quan_Ly_Sinh_Vien.Class;
using System.Data.SqlClient;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLLOPHP : Form
    {
        public frmQLLOPHP()
        {
            InitializeComponent();
        }

        DataTable tblLHP;
        private void frmQLLOPHP_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
            txtMaLopHP.Enabled = false;
            string sql1 = "select MaMon, TenMon from tblMON";
            Function.FillCombo(sql1, cboMaMonHoc, "TenMon", "MaMon");
            cboMaMonHoc.SelectedIndex = -1;
            string sql2 = "select MaGV, TenGV from tblGiang_Vien";
            Function.FillCombo(sql2, cboMaGV, "TenGV", "MaGV");
            cboMaGV.SelectedIndex = -1;
            string sql3 = "select MaLop, TenLop from tblLOP";
            Function.FillCombo(sql3, cboMaLop, "TenLop", "MaLop");
            cboMaLop.SelectedIndex = -1;
            txtTenMH.ReadOnly = true;
            txtTenGV.ReadOnly = true;
            txtTenLop.ReadOnly = true;
            LoadDataGridView();
        }

        private void cboMaMonHoc_TextChanged(object sender, EventArgs e)
        {
            if (cboMaMonHoc.Text == "")
                txtTenMH.Text = "";
            string sql = "select TenMon from tblMON where MaMon = N'" + cboMaMonHoc.Text + "'";
            txtTenMH.Text = Function.GetFileValue(sql);
        }

        private void cboMaGV_TextChanged(object sender, EventArgs e)
        {
            if (cboMaGV.Text == "")
                txtTenGV.Text = "";
            string sql = "select TenGV from tblGiang_Vien where MaGV = N'" + cboMaGV.Text + "'";
            txtTenGV.Text = Function.GetFileValue(sql);
        }

        private void LoadDataGridView()
        {
            string sql = "select a.MaLopHP, a.SoDVHT, a.SoTC, a.HocKi, a.NamBD, a.NamKT, b.TenMon , c.TenGV, d.TenLop from tblLOP_HOC_PHAN as a, tblMON as b, tblGIANG_VIEN as c, tblLOP as d where a.MaMon = b.MaMon and a.MaGV = c.MaGV and a.MaLop = d.MaLop";
            tblLHP = Function.GetDataToTable(sql);
            dgvLHP.DataSource = tblLHP;
            dgvLHP.Columns[0].HeaderText = "Mã lớp hp";
            dgvLHP.Columns[1].HeaderText = "Số DVHT";
            dgvLHP.Columns[2].HeaderText = "Số tín chỉ";
            dgvLHP.Columns[3].HeaderText = "Học kỳ";
            dgvLHP.Columns[4].HeaderText = "Năm bắt đầu";
            dgvLHP.Columns[5].HeaderText = "Năm kết thúc";
            dgvLHP.Columns[6].HeaderText = "Tên môn học";
            dgvLHP.Columns[7].HeaderText = "Tên giáo viên";
            dgvLHP.Columns[8].HeaderText = "Tên lớp";
            dgvLHP.AllowUserToAddRows = false;
            dgvLHP.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            txtMaLopHP.Enabled = true;
            txtMaLopHP.Focus();
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox     
        }

        private void ResetValue()
        {
            cboMaMonHoc.Text = "";
            txtTenMH.Text = "";
            cboMaGV.Text = "";
            txtTenGV.Text = "";
            cboMaLop.Text = "";
            txtMaLopHP.Text = "";
            txtSoDVHT.Text = "";
            txtSoTC.Text = "";
            cboHocKi.Text = "";
            txtNamBD.Text = "";
            txtNamKT.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (txtTenMH.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên môn học!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaLop.Focus();
                return;
            }
            if (txtTenGV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên giảng viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaGV.Focus();
                return;
            }
            if(txtTenLop.Text == "")
            {
                MessageBox.Show("Bạn phải nhập tên lớp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaLop.Focus();
                return;
            }    
            if (txtMaLopHP.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã lớp học phần!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaLopHP.Focus();
                return;
            }

            if (txtSoDVHT.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số đơn vị học trình!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoDVHT.Focus();
                return;
            }
            if (txtSoTC.Text == "")
            {
                MessageBox.Show("Bạn phải nhập số tín chỉ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoTC.Focus();
                return;
            }
            if (cboHocKi.Text == "")
            {
                MessageBox.Show("Bạn phải nhập học kì!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboHocKi.Focus();
                return;
            }
            if (txtNamBD.Text == "")
            {
                MessageBox.Show("Bạn phải nhập năm bắt đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNamBD.Focus();
                return;
            }
            if (txtNamKT.Text == "")
            {
                MessageBox.Show("Bạn phải nhập năm kết thúc!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNamKT.Focus();
                return;
            }

            sql = "Select MaLopHP From tblLOP_HOC_PHAN where MaLopHP = N'" + txtMaLopHP.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Mã lớp học phần này đã có, bạn phải nhập mã lớp học phần khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMaLopHP.Focus();
                return;
            }

            sql = "INSERT INTO tblLOP_HOC_PHAN VALUES(N'" + txtMaLopHP.Text + "', N'" + txtSoDVHT.Text + "', N'" + txtSoTC.Text + "', N'" + cboHocKi.Text + "', N'" + txtNamBD.Text + "',N'" + txtNamKT.Text + "', N'"+cboMaMonHoc.Text+"', N'"+cboMaGV.Text+"', N'"+cboMaLop.Text+"')";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaLopHP.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql; //Lưu lệnh sql
            if (txtTenMH.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên môn học!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaMonHoc.Focus();
                return;
            }
            if (txtTenGV.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên giảng viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaGV.Focus();
                return;
            }
            if (txtTenLop.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên giảng viên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaLop.Focus();
                return;
            }
            if (txtMaLopHP.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã lớp học phần!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaLopHP.Focus();
                return;
            }

            if (txtSoDVHT.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số đơn vị học trình!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoDVHT.Focus();
                return;
            }
            if (txtSoTC.Text == "")
            {
                MessageBox.Show("Bạn phải nhập số tín chỉ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSoTC.Focus();
                return;
            }
            if (cboHocKi.Text == "")
            {
                MessageBox.Show("Bạn phải nhập học kì!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboHocKi.Focus();
                return;
            }
            if (txtNamBD.Text == "")
            {
                MessageBox.Show("Bạn phải nhập năm bắt đầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNamBD.Focus();
                return;
            }
            if (txtNamKT.Text == "")
            {
                MessageBox.Show("Bạn phải nhập năm kết thúc!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNamKT.Focus();
                return;
            }
            sql = "UPDATE tblLOP_HOC_PHAN SET MaMon = '"+cboMaMonHoc.Text+"', MaGV = '"+cboMaGV.Text+"',  SoDVHT = N'" + txtSoDVHT.Text + "', SoTC = '" + txtSoTC.Text + "', HocKi = N'" + cboHocKi.Text + "', NamBD = N'" + txtNamBD.Text + "', NamKT = '" + txtNamKT.Text + "', MaLop = '"+cboMaLop.Text+"' Where MaLopHP = '" + txtMaLopHP.Text + "'";
            Function.RunSQL(sql); //Thực hiện câu lệnh sql
            MessageBox.Show("Sửa thành công!", "Thông báo");
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaLopHP.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblLHP.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMaLopHP.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE FROM tblLOP_HOC_PHAN WHERE MaLopHP = N'" + txtMaLopHP.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
            btnThem.Enabled = true;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
        }

        private void dgvLHP_Click(object sender, EventArgs e)
        {
            string mamon, magv, malop;
            string sql = "select mamon from tblLOP_HOC_PHAN where MaLopHP = '"+dgvLHP.CurrentRow.Cells["MaLopHP"].Value.ToString()+"'";
            mamon = Function.GetFileValue(sql);
            cboMaMonHoc.Text = mamon.ToString();
            string sql1 = "select magv from tblLOP_HOC_PHAN where MaLopHP = '" + dgvLHP.CurrentRow.Cells["MaLopHP"].Value.ToString() + "'";
            magv = Function.GetFileValue(sql1);
            cboMaGV.Text = magv.ToString();
            string sql2 = "select malop from tblLOP_HOC_PHAN where MaLopHP = '" + dgvLHP.CurrentRow.Cells["MaLopHP"].Value.ToString() + "'";
            malop = Function.GetFileValue(sql2);
            cboMaLop.Text = malop.ToString();
            //txtTenMH.Text = dgvLHP.CurrentRow.Cells["TenMon"].Value.ToString();
            //txtTenGV.Text = dgvLHP.CurrentRow.Cells["TenGV"].Value.ToString();
            txtMaLopHP.Text = dgvLHP.CurrentRow.Cells["MaLopHP"].Value.ToString();
            txtSoDVHT.Text = dgvLHP.CurrentRow.Cells["SoDVHT"].Value.ToString();
            txtSoTC.Text = dgvLHP.CurrentRow.Cells["SoTC"].Value.ToString();
            cboHocKi.Text = dgvLHP.CurrentRow.Cells["HocKi"].Value.ToString();
            txtNamBD.Text = dgvLHP.CurrentRow.Cells["NamBD"].Value.ToString();
            txtNamKT.Text = dgvLHP.CurrentRow.Cells["NamKT"].Value.ToString();
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoQua.Enabled = true;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            txtMaLopHP.Enabled = false;
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            btnThem.Enabled = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboMaLop_TextChanged(object sender, EventArgs e)
        {
            if (cboMaLop.Text == "")
                txtTenLop.Text = "";
            string sql = "select TenLop from tblLOP where MaLop = N'" + cboMaLop.Text + "'";
            txtTenLop.Text = Function.GetFileValue(sql);
        }


    }
}
