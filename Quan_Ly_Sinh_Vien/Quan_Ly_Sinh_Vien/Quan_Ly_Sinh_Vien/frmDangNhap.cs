﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;
using System.Diagnostics;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmDangNhap : Form
    {
        public frmDangNhap()
        {
            InitializeComponent();
        }      
        private void textBox1_Click(object sender, EventArgs e)
        {
            txtUsername.Clear();
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            txtUsername.Text = "UserName";
            txtPassWord.Text = "PassWord";            
        }

        private void txtPassWord_Click(object sender, EventArgs e)
        {
            txtPassWord.Clear();
            txtPassWord.UseSystemPasswordChar = true;
        }

        private void lblClose_MouseHover(object sender, EventArgs e)
        {
            lblClose.ForeColor = Color.Red;
        }

        private void lblClose_MouseLeave(object sender, EventArgs e)
        {
            lblClose.ForeColor = Color.CornflowerBlue;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {       
            if(txtUsername.Text == "")
            {
                MessageBox.Show("Vui lòng nhập tên tài khoản!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsername.Focus();
                return;
            }    
            if(txtPassWord.Text == "")
            {
                MessageBox.Show("Vui lòng nhập mật khẩu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassWord.Focus();
                return;
            }    
            Function.Connect();
            string sql = "Select * From tblLOGIN where TenDN ='" + txtUsername.Text + "' and MatKhau ='" + txtPassWord.Text + "' and MaQuyen='1'";
            SqlCommand cmd = new SqlCommand(sql, Function.conn);
            SqlDataReader reader;
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                MessageBox.Show("Đăng nhập vào hệ thống !", "Thông báo !");
                frmMain frm = new frmMain();
                frm.Show();
                this.Visible = false;
                frm.mnuDangNhap.Enabled = false;
                cmd.Dispose();
                reader.Close();
                reader.Dispose();
            }
            else
            {
                cmd.Dispose();
                reader.Close();
                reader.Dispose();
                string sql1 = "Select * From tblLOGIN where TenDN='" + txtUsername.Text + "' and MatKhau ='" + txtPassWord.Text + "' and MaQuyen='2'";
                SqlCommand cmd1 = new SqlCommand(sql1,Function.conn);
                SqlDataReader reader1;
                reader1 = cmd1.ExecuteReader();

                if (reader1.Read())
                {
                    MessageBox.Show("Đăng nhập vào hệ thống !", "Thông báo !");

                    frmMain frm = new frmMain();
                    frm.Show();
                    frm.mnuDangNhap.Enabled = false;
                    frm.mnuQLNguoiDung.Enabled = false;
                    frm.mnuSinhVien.Enabled = false;
                    frm.mnuGiangVien.Enabled = false;
                    frm.mnuDangNhap.Enabled = false;
                    this.Visible = false;
                    frm.btnGiangVien.Hide();
                    frm.btnQLKhoa.Hide();
                    frm.btnQLLop.Hide();
                    frm.btnQLSinhVien.Hide();
                    frm.btnQLMonHoc.Hide();
                    frm.btnQLLopHP.Hide();
                    frm.mnuDanhMuc.Enabled = false;
                    frm.btnQLDiem.Hide();
                    frm.mnuDiemHK.Enabled = false;
                    cmd1.Dispose();
                    reader1.Close();
                    reader1.Dispose();
                }
                else
                {
                    cmd1.Dispose();
                    reader1.Close();
                    reader1.Dispose();
                    string sql2 = "Select * From tblLOGIN where TenDN='" + txtUsername.Text + "' and MatKhau ='" + txtPassWord.Text + "' and MaQuyen='3'";
                    SqlCommand cmd2 = new SqlCommand(sql2, Function.conn);
                    SqlDataReader reader2;
                    reader2 = cmd2.ExecuteReader();
                    if (reader2.Read())
                    {
                        MessageBox.Show("Đăng nhập vào hệ thống !", "Thông báo !");

                        frmMain frm = new frmMain();
                        frm.Show();
                        frm.mnuDangNhap.Enabled = false;
                        frm.mnuQLNguoiDung.Enabled = false;
                        frm.mnuQuanLy.Enabled = false;
                        this.Visible = false ;
                        frm.mnuDangNhap.Enabled = false;
                        frm.btnGiangVien.Hide();
                        frm.btnQLKhoa.Hide();
                        frm.btnQLLop.Hide();
                        frm.btnQLSinhVien.Hide();
                        frm.btnQLMonHoc.Hide();
                        frm.btnQLDiem.Hide();
                        frm.btnQLLopHP.Hide();
                        frm.mnuDanhMuc.Enabled = false;
                        cmd2.Dispose();
                        reader2.Close();
                        reader2.Dispose();
                    }
                    else
                    {
                        MessageBox.Show("Tên đăng nhập hoặc mật khẩu sai !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }    
                }
                
            }
            
        }
        
        private void txtPassWord_TextChanged(object sender, EventArgs e)
        {
            if (txtPassWord.Text != "PassWord")
            {
                txtPassWord.UseSystemPasswordChar = true;
            }
        }
    }
}
