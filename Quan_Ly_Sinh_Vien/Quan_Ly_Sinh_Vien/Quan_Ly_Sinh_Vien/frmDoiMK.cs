﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmDoiMK : Form
    {
        public frmDoiMK()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string select2 = "Select * From tblLOGIN where TenDN='" + txtTenTK.Text + "' and MatKhau ='" + txtMatKhauCu.Text + "'";
            SqlCommand cmd2 = new SqlCommand(select2, Function.conn);
            SqlDataReader reader2;
            reader2 = cmd2.ExecuteReader();

            errorProvider1.Clear();
            if (txtTenTK.Text == "")
                errorProvider1.SetError(txtTenTK, "Chưa nhập tên tài khoản !");
            else if (txtMatKhauCu.Text == "")
            {
                errorProvider1.SetError(txtMatKhauCu, "Vui lòng nhập mật khẩu !");
                txtMatKhauCu.Focus();
            }
            else if (txtMatKhauMoi.Text == "")
            {
                errorProvider1.SetError(txtMatKhauMoi, "Vui lòng nhập mật khẩu mới");
                txtMatKhauMoi.Focus();
            }
            else if (txtNhapLaiMK.Text == "")
            {
                errorProvider1.SetError(txtNhapLaiMK, "Vui lòng nhập lại mật khẩu!");
                txtNhapLaiMK.Focus();
            }
            else if (txtNhapLaiMK.Text != txtMatKhauMoi.Text)
            {
                MessageBox.Show("Bạn nhập lại password không trùng khớp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNhapLaiMK.Focus();
                return;
            }
            else if (reader2.Read())
            {
                cmd2.Dispose();
                reader2.Dispose();
                // Thực hiện truy vấn
                string sql = "Update tblLOGIN Set MatKhau ='" + txtMatKhauMoi.Text + "' where TenDN='" + txtTenTK.Text + "'";
                Function.RunSQL(sql);
                MessageBox.Show("Cập nhật dữ liệu thành công", "Thông báo!");

                // Trả tài nguyên
            }

            else
            {
                MessageBox.Show("Tên tài khoản không tồn tại hoặc mật khẩu sai! ", "Thông báo !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTenTK.Focus();

            }
            cmd2.Dispose();
            reader2.Dispose();
            Reset();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtTenTK.Text = "";
            txtMatKhauCu.Text = "";
            txtMatKhauMoi.Text = "";
            txtNhapLaiMK.Text = "";
            txtTenTK.Focus();
        }

        private void Reset()
        {
            txtTenTK.Text = "";
            txtMatKhauCu.Text = "";
            txtMatKhauMoi.Text = "";
            txtNhapLaiMK.Text = "";
            txtTenTK.Focus();
        }
    }
}
