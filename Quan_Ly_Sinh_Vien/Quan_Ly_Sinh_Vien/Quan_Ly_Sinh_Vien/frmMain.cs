﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnQLSinhVien_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLSV frm = new frmQLSV();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnQLLop_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLLop frm = new frmQLLop();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void btnGiangVien_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLGiangVien frm = new frmQLGiangVien();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void btnQLKhoa_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLKhoa frm = new frmQLKhoa();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void btnQLMonHoc_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLMonHoc frm = new frmQLMonHoc();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuDoiMK_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmDoiMK frm = new frmDoiMK();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void btnQLDiem_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLDiemSV frm = new frmQLDiemSV();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuThoat_Click(object sender, EventArgs e)
        {
           if(MessageBox.Show("Bạn có muốn thoát không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
           {
               Application.Exit();
           }
        }

        private void btnQLLopHP_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLLOPHP frm = new frmQLLOPHP();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;           
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLDiemSVTheoHocKi frm = new frmQLDiemSVTheoHocKi();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mnuDangXuat_Click(object sender, EventArgs e)
        {

            frmDangNhap frm = new frmDangNhap();
            frm.Show();
            this.Close();         
        }

        private void mnuQLNguoiDung_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmNguoiDung frm = new frmNguoiDung();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuThongTinSV_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmTKSV frm = new frmTKSV();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void dToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmTKDiem frm = new frmTKDiem();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuDiemMH_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLDiemSV frm= new frmQLDiemSV();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuGiangVien_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLGiangVien frm = new frmQLGiangVien();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuSinhVien_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLSV frm = new frmQLSV();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuMonHoc_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLMonHoc frm = new frmQLMonHoc();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuKhoa_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLKhoa frm = new frmQLKhoa();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuLop_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmQLLop frm = new frmQLLop();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuThongKe_Click(object sender, EventArgs e)
        {

        }

        private void mnuDanhSachSV_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmBCSVDuocHocBong frm = new frmBCSVDuocHocBong();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }

        private void mnuDiemTK_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            frmBCSVHocLai frm = new frmBCSVHocLai();
            frm.MdiParent = this;
            frm.Show();
            frm.Left = 0;
            frm.Top = 0;
        }
    }
}
