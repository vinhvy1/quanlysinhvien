﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmTKSV : Form
    {
        public frmTKSV()
        {
            InitializeComponent();
        }
        DataTable tblSV;
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            if (txtMaSV.Text == "")
            {
                MessageBox.Show("Hãy nhập điều kiện tìm kiếm!!!", "Yêu cầu ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string sql = "select a.MaSV, a.HoTen, a.NgaySinh, a.GioiTinh, a.DiaChi, b.TenLop, c.TenKhoa from tblSINH_VIEN as a, tblLOP as b, tblKHOA as c where a.MaLop = b.MaLop and b.MaKhoa = c.MaKhoa";
            if (txtMaSV.Text != "")
                sql = sql + " AND a.MaSV Like N'%" + txtMaSV.Text + "%'";
            tblSV = Function.GetDataToTable(sql);
            if (tblSV.Rows.Count == 0)
            {
                MessageBox.Show("Không có bản ghi thỏa mãn điều kiện!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Có " + tblSV.Rows.Count + " bản ghi thỏa mãn điều kiện!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dgvSV.DataSource = tblSV ;
            dgvSV.Columns[0].HeaderText = "Mã sinh viên";
            dgvSV.Columns[1].HeaderText = "Họ tên";
            dgvSV.Columns[2].HeaderText = "Ngày sinh";
            dgvSV.Columns[3].HeaderText = "Giới tính";
            dgvSV.Columns[4].HeaderText = "Địa chỉ";
            dgvSV.Columns[5].HeaderText = "Lớp";
            dgvSV.Columns[6].HeaderText = "Khoa";
            dgvSV.AllowUserToAddRows = false;
            dgvSV.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
