﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;
using Microsoft.Reporting.WinForms;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmBCSVHocLai : Form
    {
        public frmBCSVHocLai()
        {
            InitializeComponent();
        }

        private void frmBCSVHocLai_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void btnTaoBC_Click(object sender, EventArgs e)
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "BCSVHocLai";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = Function.conn;
            cmd.Parameters.Add(new SqlParameter("@HocKi", cboHocKi.Text));
            cmd.Parameters.Add(new SqlParameter("@NamBD", cboNamBD.Text));
            DataSet ds = new DataSet();
            SqlDataAdapter dap = new SqlDataAdapter(cmd);
            dap.Fill(ds);
            reportViewer1.ProcessingMode = ProcessingMode.Local;
            reportViewer1.LocalReport.ReportEmbeddedResource = "Quan_Ly_Sinh_Vien.rptBCDSHocLai.rdlc";
            ReportDataSource rps = new ReportDataSource();
            rps.Name = "dsHocLai";
            rps.Value = ds.Tables[0];
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(rps);
            reportViewer1.RefreshReport();
        }

        private void cboNamBD_TextChanged(object sender, EventArgs e)
        {
            int nambd;
            if (cboNamBD.Text == "")
            {
                nambd = 0;
                cboNamBD.Text = "";
            }
            else
            {
                nambd = Convert.ToInt32(cboNamBD.Text);
                cboNamKT.Text = (nambd + 1).ToString();
            }
        }
    }
}
