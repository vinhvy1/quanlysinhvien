﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLLop : Form
    {
        public frmQLLop()
        {
            InitializeComponent();
        }

        DataTable tblLop;
        private void frmQLLop_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaLop.Enabled = false;
            LoadDataGridView();
            string sql = "Select MaKhoa, TenKhoa from tblKhoa";
            Function.FillCombo(sql, cboMaKhoa, "TenKhoa", "MaKhoa");
        }

        private void LoadDataGridView()
        {
            string sql = "Select * from tblLOP";
            tblLop = Function.GetDataToTable(sql);
            dgvLop.DataSource = tblLop;
            dgvLop.Columns[0].HeaderText = "Mã lớp";
            dgvLop.Columns[1].HeaderText = "Mã khoa";
            dgvLop.Columns[2].HeaderText = "Tên lớp";
            dgvLop.AllowUserToAddRows = false;
            dgvLop.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox
            txtMaLop.Enabled = true; //cho phép nhập mới
            txtMaLop.Focus();
        }

        private void ResetValue()
        {
            txtMaLop.Text = "";
            txtTenLop.Text = "";
            cboMaKhoa.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtMaLop.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã lớp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaLop.Focus();
                return;
            }
            if (txtTenLop.Text == "")
            {
                MessageBox.Show("Bạn phải nhập tên tên lớp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenLop.Focus();
                return;
            }
            if (cboMaKhoa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaKhoa.Focus();
                return;
            }
            sql = "Select MaLop From tblLOP where MaLop = N'" + txtMaLop.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Mã lớp này đã có, bạn phải nhập mã khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMaLop.Focus();
                return;
            }

            sql = "INSERT INTO tblLOP Values(N'" + txtMaLop.Text + "' , N'" + cboMaKhoa.Text + "', N'"+txtTenLop.Text+"')";
            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaLop.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblLop.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMaLop.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE tblLOP WHERE MaLop = N'" + txtMaLop.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
        }

        private void dgvLop_Click(object sender, EventArgs e)
        {
            btnThem.Enabled = false;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoQua.Enabled = true;
            txtMaLop.Text = dgvLop.CurrentRow.Cells["MaLop"].Value.ToString();
            txtTenLop.Text = dgvLop.CurrentRow.Cells["TenLop"].Value.ToString();
            cboMaKhoa.Text = dgvLop.CurrentRow.Cells["MaKhoa"].Value.ToString();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtMaLop.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã lớp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaLop.Focus();
                return;
            }
            if (txtTenLop.Text == "")
            {
                MessageBox.Show("Bạn phải nhập tên tên lớp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenLop.Focus();
                return;
            }
            if (cboMaKhoa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenLop.Focus();
                return;
            }

            sql = "update tblLOP set MaKhoa = '" + cboMaKhoa.Text + "', TenLop = N'" + txtTenLop.Text + "' where MaLop = '" + txtMaLop.Text + "'";
            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaLop.Enabled = false;
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaLop.Enabled = false;
            btnThem.Enabled = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
