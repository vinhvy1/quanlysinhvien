﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Quan_Ly_Sinh_Vien.Class;

namespace Quan_Ly_Sinh_Vien
{
    public partial class frmQLMonHoc : Form
    {
        public frmQLMonHoc()
        {
            InitializeComponent();
        }

        DataTable tblMonHoc;
        
        private void frmQLMonHoc_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaMon.Enabled = false;
            LoadDataGridView();
            string sql = "Select MaKhoa, TenKhoa from tblKhoa";
            Function.FillCombo(sql, cboMaKhoa, "TenKhoa", "MaKhoa");
        }

        private void LoadDataGridView()
        {
            string sql = "Select * from tblMON";
            tblMonHoc = Function.GetDataToTable(sql);
            dgvMonHoc.DataSource = tblMonHoc;
            dgvMonHoc.Columns[0].HeaderText = "Mã môn học";
            dgvMonHoc.Columns[1].HeaderText = "Tên môn học";
            dgvMonHoc.Columns[2].HeaderText = "Mã khoa";
            dgvMonHoc.AllowUserToAddRows = false;
            dgvMonHoc.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnBoQua.Enabled = true;
            btnLuu.Enabled = true;
            btnThem.Enabled = false;
            ResetValue(); //Xoá trắng các textbox
            txtMaMon.Enabled = true; //cho phép nhập mới
            txtMaMon.Focus();
        }

        private void ResetValue()
        {
            txtMaMon.Text = "";
            txtTenMon.Text = "";
            cboMaKhoa.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtMaMon.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã môn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaMon.Focus();
                return;
            }
            if (txtTenMon.Text == "")
            {
                MessageBox.Show("Bạn phải nhập tên môn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenMon.Focus();
                return;
            }
            if (cboMaKhoa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaKhoa.Focus();
                return;
            }
            sql = "Select MaLop From tblLOP where MaLop = N'" + txtMaMon.Text.Trim() + "'";
            if (Function.CheckKey(sql))
            {
                MessageBox.Show("Mã lớp này đã có, bạn phải nhập mã khác!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMaMon.Focus();
                return;
            }

            sql = "INSERT INTO tblMON Values(N'" + txtMaMon.Text + "' , N'" + txtTenMon.Text + "', N'" + cboMaKhoa.Text + "')";
            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaMon.Enabled = false;
        }

        private void dgvMonHoc_Click(object sender, EventArgs e)
        {
            btnThem.Enabled = false;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnBoQua.Enabled = true;
            txtMaMon.Text = dgvMonHoc.CurrentRow.Cells["MaMon"].Value.ToString();
            txtTenMon.Text = dgvMonHoc.CurrentRow.Cells["TenMon"].Value.ToString();
            cboMaKhoa.Text = dgvMonHoc.CurrentRow.Cells["MaKhoa"].Value.ToString();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string sql;
            if (txtMaMon.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã môn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMaMon.Focus();
                return;
            }
            if (txtTenMon.Text == "")
            {
                MessageBox.Show("Bạn phải nhập tên môn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTenMon.Focus();
                return;
            }
            if (cboMaKhoa.Text == "")
            {
                MessageBox.Show("Bạn phải nhập mã khoa!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboMaKhoa.Focus();
                return;
            }
            sql = "update tblMON set TenMon = N'" + txtTenMon.Text + "', MaKhoa = '" + cboMaKhoa.Text + "' where MaMon = '" + txtMaMon.Text + "'";
            Function.RunSQL(sql);
            LoadDataGridView(); //Nạp lại DataGridView
            ResetValue();
            btnXoa.Enabled = true;
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnBoQua.Enabled = false;
            btnLuu.Enabled = false;
            txtMaMon.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql;
            if (tblMonHoc.Rows.Count == 0)
            {
                MessageBox.Show("Không còn dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMaMon.Text == "")
            {
                MessageBox.Show("Bạn chưa chọn bản ghi nào", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("Bạn có muốn xoá bản ghi này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                sql = "DELETE tblMON WHERE MaMon = N'" + txtMaMon.Text + "'";
                Function.RunSQL(sql);
                LoadDataGridView();
                ResetValue();
            }
        }

        private void btnBoQua_Click(object sender, EventArgs e)
        {
            ResetValue();
            btnLuu.Enabled = false;
            btnBoQua.Enabled = false;
            txtMaMon.Enabled = false;
            btnThem.Enabled = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
